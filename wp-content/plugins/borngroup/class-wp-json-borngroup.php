<?php
/*
Plugin Name: Borngroup
Plugin URI: http://www.borngroup.com/
Description: Customization Development
Version: 1.0.0
Author: Born Group
Author URI: http://www.borngroup.com/
License: GPL
Copyright: Born Group Inc.
*/

function born_api_init() {
    global $born_api_posts;

    $born_api_posts = new Born_API_Posts();
    add_filter( 'json_endpoints', array( $born_api_posts, 'register_routes' ) );
}

add_action( 'wp_json_server_before_serve', 'born_api_init' );


class Born_API_Posts {
	
    static $number_per_post = 5;

	public function register_routes( $routes ) {

		$routes['/born/page/home/(?P<page>\S+)'] = array(
            array( array( $this, 'get_page_home'), WP_JSON_Server::READABLE ),
        );

        $routes['/born/menus'] = array(
            array( array( $this, 'get_wp_menus'), WP_JSON_Server::READABLE ),
        );

        $routes['/born/categories'] = array(
            array( array( $this, 'get_categories'), WP_JSON_Server::READABLE ),
        );

        $routes['/born/page/category/(?P<catId>\d+)/(?P<page>\d+)'] = array(
            array( array( $this, 'get_page_category'), WP_JSON_Server::READABLE ),
        );

        $routes['/born/posts'] = array(
            array( array( $this, 'get_posts'), WP_JSON_Server::READABLE ),
        );

        $routes['/born/posts/(?P<slug>\S+)'] = array(
            array( array( $this, 'get_post' ),       WP_JSON_Server::READABLE ),
            array( array( $this, 'edit_post' ),      WP_JSON_Server::EDITABLE | WP_JSON_Server::ACCEPT_JSON ),
            array( array( $this, 'delete_post' ),    WP_JSON_Server::DELETABLE ),
        );

        return $routes;
	}

	function get_page_home( $page ) {
		//$args = array( 'numberposts' => $number_per_post, 'post_status'=>"publish",'post_type'=>"post",'orderby'=>"post_date");
		//return $this->get_page_posts($args);

        $paged = $page ? $page : 1;
        $args = array(
            'paged' => $paged,
            'posts_per_page' => 5,
            'post_status'=> 'publish',
            'post_type' => 'post',
            'orderby'=> 'post_date'
        );

        return $this->get_page_posts($args);
    }

    function get_page_posts( $args ) {
        $posts = query_posts( $args );

        $args = array(
            'post_status'=> 'publish',
            'post_type' => 'post',
            'orderby'=> 'post_date'
        );

        $total_posts = query_posts( $args );

        $post_array = array();

        foreach ($posts as &$post) {
            $post = $this->format_post($post);
            $post->featured_image = new stdClass();
            $featured_image_id = get_post_thumbnail_id($post->ID);
            $post->featured_image->url = wp_get_attachment_url($featured_image_id) ? wp_get_attachment_url($featured_image_id) : '';
            $post_array[] = $post;
        }

        $page = pods('page', array());
        $related_products = $page->field('related_products');
        
        
        $obj = new stdClass();
        $obj->posts = $post_array;
        $obj->products = $related_products;
        $obj->totalCnt = count($total_posts);

        $response   = new WP_JSON_Response();
        $response->set_data($obj);

        return $response;
    }	

    function get_wp_menus() {
        $menus = wp_list_cats('sort_column=name&optioncount=1&hide_empty=0&hierarchical=1');

        $obj = new stdClass();
        $obj->menus = $menus;

        $response   = new WP_JSON_Response();
        $response->set_data($obj);

        return $response;
    }

    function get_categories() {
        $categories = get_categories();

        $categories = $this->object_to_array($categories);

        $response   = new WP_JSON_Response();
        $response->set_data( $categories);

        return $response;
    }

    function get_page_category( $catId, $page ) {
        //$args = array( 'numberposts' => $number_per_post, 'category' => $catId, 'post_status'=>"publish",'post_type'=>"post",'orderby'=>"post_date");
        
        $paged = $page ? $page : 1;
        $args = array(
            'paged' => $paged,
            'posts_per_page' => 5,
            'category' => $catId,
            'post_status'=> 'publish',
            'post_type' => 'post',
            'orderby'=> 'post_date'
        );

        return $this->get_page_posts($args);
    }

    function get_posts( $filter = array(), $context = 'view', $type = 'post', $page = 1 ) {
        $query = array();

        // Validate post types and permissions
        $query['post_type'] = array();

        foreach ( (array) $type as $type_name ) {
            $post_type = get_post_type_object( $type_name );

            if ( ! ( (bool) $post_type ) || ! $post_type->show_in_json ) {
                return new WP_Error( 'json_invalid_post_type', sprintf( __( 'The post type "%s" is not valid' ), $type_name ), array( 'status' => 403 ) );
            }

            $query['post_type'][] = $post_type->name;
        }

        global $wp;

        // Allow the same as normal WP
        $valid_vars = apply_filters('query_vars', $wp->public_query_vars);

        // If the user has the correct permissions, also allow use of internal
        // query parameters, which are only undesirable on the frontend
        //
        // To disable anyway, use `add_filter('json_private_query_vars', '__return_empty_array');`

        if ( current_user_can( $post_type->cap->edit_posts ) ) {
            $private = apply_filters( 'json_private_query_vars', $wp->private_query_vars );
            $valid_vars = array_merge( $valid_vars, $private );
        }

        // Define our own in addition to WP's normal vars
        $json_valid = array( 'posts_per_page' );
        $valid_vars = array_merge( $valid_vars, $json_valid );

        // Filter and flip for querying
        $valid_vars = apply_filters( 'json_query_vars', $valid_vars );
        $valid_vars = array_flip( $valid_vars );

        // Exclude the post_type query var to avoid dodging the permission
        // check above
        unset( $valid_vars['post_type'] );

        $post_query = new WP_Query();
        $posts_list = $post_query->query( $query );
        $result_cnt = count($posts_list);

        foreach ( $valid_vars as $var => $index ) {
            if ( isset( $filter[ $var ] ) ) {
                $query[ $var ] = apply_filters( 'json_query_var-' . $var, $filter[ $var ] );
            }
        }

        // Special parameter handling
        $query['paged'] = absint( $page );

        $post_query = new WP_Query();
        $posts_list = $post_query->query( $query );
        $response   = new WP_JSON_Response();
        $response->query_navigation_headers( $post_query );

        if ( ! $posts_list ) {
            $response->set_data( array() );
            return $response;
        }

        // holds all the posts data
        $struct = array();

        $response->header( 'Last-Modified', mysql2date( 'D, d M Y H:i:s', get_lastpostmodified( 'GMT' ), 0 ).' GMT' );

        $obj = new stdClass();
        
        foreach ( $posts_list as $post ) {
            $post = get_object_vars( $post );

            // Do we have permission to read this post?
            if ( ! json_check_post_permission( $post, 'read' ) ) {
                continue;
            }

            $response->link_header( 'item', json_url( '/posts/' . $post['ID'] ), array( 'title' => $post['post_title'] ) );
            $post_data = $this->prepare_post( $post, $context );
            if ( is_wp_error( $post_data ) ) {
                continue;
            }

            $struct[] = $post_data;
        }
        $obj->cnt = $result_cnt;
        $obj->posts = $struct;

        $response->set_data( $obj );

        return $response;
    }

    /**
     * Retrieve a post.
     *
     * @uses get_post()
     * @param int $id Post ID
     * @param string $context The context; 'view' (default) or 'edit'.
     * @return array Post entity
     */
    public function get_post( $slug, $context = 'view' ) {
        #$id = (int) $id;
        #$post = get_post( $id, ARRAY_A );
        $args = array(
            'name'        => $slug,
            'post_type'   => 'post',
            'post_status' => 'publish',
            'numberposts' => 1
        );

        $posts = get_posts($args);  
        $post = get_object_vars($posts[0]);

        if ( empty( $slug ) || empty( $post['ID'] ) ) {
            return new WP_Error( 'json_post_invalid_slug', __( 'Invalid post Slug.' ), array( 'status' => 404 ) );
        }
        $id = $post['ID'];

        $checked_permission = 'read';
        if ( 'inherit' === $post['post_status'] && $post['post_parent'] > 0 ) {
            $checked_post = get_post( $post['post_parent'], ARRAY_A );
            if ( 'revision' === $post['post_type'] ) {
                $checked_permission = 'edit';
            }
        } else {
            $checked_post = $post;
        }

        if ( ! json_check_post_permission( $checked_post, $checked_permission ) ) {
            return new WP_Error( 'json_user_cannot_read', __( 'Sorry, you cannot read this post.' ), array( 'status' => 401 ) );
        }

        // Link headers (see RFC 5988)

        $response = new WP_JSON_Response();
        $response->header( 'Last-Modified', mysql2date( 'D, d M Y H:i:s', $post['post_modified_gmt'] ) . 'GMT' );

        $post = $this->prepare_post( $post, $context );

        if ( is_wp_error( $post ) ) {
            return $post;
        }

        foreach ( $post['meta']['links'] as $rel => $url ) {
            $response->link_header( $rel, $url );
        }

        $response->link_header( 'alternate',  get_permalink( $id ), array( 'type' => 'text/html' ) );
        $response->set_data( $post );

        return $response;
    }

    /**
     * Prepares post data for return in an XML-RPC object.
     *
     * @access protected
     *
     * @param array $post The unprepared post data
     * @param string $context The context for the prepared post. (view|view-revision|edit|embed|single-parent)
     * @return array The prepared post data
     */
    protected function prepare_post( $post, $context = 'view' ) {
        // Holds the data for this post.
        $_post = array( 'ID' => (int) $post['ID'] );

        $post_type = get_post_type_object( $post['post_type'] );

        if ( ! json_check_post_permission( $post, 'read' ) ) {
            return new WP_Error( 'json_user_cannot_read', __( 'Sorry, you cannot read this post.' ), array( 'status' => 401 ) );
        }

        $previous_post = null;
        if ( ! empty( $GLOBALS['post'] ) ) {
            $previous_post = $GLOBALS['post'];
        }
        $post_obj = get_post( $post['ID'] );

        // Don't allow unauthenticated users to read password-protected posts
        if ( ! empty( $post['post_password'] ) ) {
            if ( ! json_check_post_permission( $post, 'edit' ) ) {
                return new WP_Error( 'json_user_cannot_read', __( 'Sorry, you cannot read this post.' ), array( 'status' => 403 ) );
            }

            // Fake the correct cookie to fool post_password_required().
            // Without this, get_the_content() will give a password form.
            require_once ABSPATH . 'wp-includes/class-phpass.php';
            $hasher = new PasswordHash( 8, true );
            $value = $hasher->HashPassword( $post['post_password'] );
            $_COOKIE[ 'wp-postpass_' . COOKIEHASH ] = wp_slash( $value );
        }

        $GLOBALS['post'] = $post_obj;
        setup_postdata( $post_obj );

        // prepare common post fields
        $post_fields = array(
            'title'           => get_the_title( $post['ID'] ), // $post['post_title'],
            'status'          => $post['post_status'],
            'type'            => $post['post_type'],
            'author'          => (int) $post['post_author'],
            'post_author'     => pods_field($post_obj->post_type, $post_obj->ID, 'custom_author', true),
            'post_date'       => pods_field($post_obj->post_type, $post_obj->ID, 'post_custom_date', true),
            'content'         => $post['post_content'],
            'parent'          => (int) $post['post_parent'],
            #'post_mime_type' => $post['post_mime_type'],
            'link'            => get_permalink( $post['ID'] ),
            'shop_this_story'  => pods_field($post_obj->post_type, $post_obj->ID, 'shop_this_story', true),
            'category_recent_posts'    => $this->get_category_recent_posts( $post['ID']),
            'global_recent_posts'    => $this->get_global_recent_posts( $post['ID']),
        );

        $post_fields_extended = array(
            'slug'           => $post['post_name'],
            'guid'           => apply_filters( 'get_the_guid', $post['guid'] ),
            'excerpt'        => $this->prepare_excerpt( $post['post_excerpt'] ),
            'menu_order'     => (int) $post['menu_order'],
            'comment_status' => $post['comment_status'],
            'ping_status'    => $post['ping_status'],
            'sticky'         => ( $post['post_type'] === 'post' && is_sticky( $post['ID'] ) ),
        );

        $post_fields_raw = array(
            'title_raw'   => $post['post_title'],
            'content_raw' => $post['post_content'],
            'excerpt_raw' => $post['post_excerpt'],
            'guid_raw'    => $post['guid'],
        );

        // Dates
        $timezone = json_get_timezone();

        if ( $post['post_date_gmt'] === '0000-00-00 00:00:00' ) {
            $post_fields['date']              = null;
            $post_fields_extended['date_tz']  = null;
            $post_fields_extended['date_gmt'] = null;
        }
        else {
            $post_date                        = WP_JSON_DateTime::createFromFormat( 'Y-m-d H:i:s', $post['post_date'], $timezone );
            $post_fields['date']              = json_mysql_to_rfc3339( $post['post_date'] );
            $post_fields_extended['date_tz']  = $post_date->format( 'e' );
            $post_fields_extended['date_gmt'] = json_mysql_to_rfc3339( $post['post_date_gmt'] );
        }

        if ( $post['post_modified_gmt'] === '0000-00-00 00:00:00' ) {
            $post_fields['modified']              = null;
            $post_fields_extended['modified_tz']  = null;
            $post_fields_extended['modified_gmt'] = null;
        }
        else {
            $modified_date                        = WP_JSON_DateTime::createFromFormat( 'Y-m-d H:i:s', $post['post_modified'], $timezone );
            $post_fields['modified']              = json_mysql_to_rfc3339( $post['post_modified'] );
            $post_fields_extended['modified_tz']  = $modified_date->format( 'e' );
            $post_fields_extended['modified_gmt'] = json_mysql_to_rfc3339( $post['post_modified_gmt'] );
        }

        // Authorized fields
        // TODO: Send `Vary: Authorization` to clarify that the data can be
        // changed by the user's auth status
        if ( json_check_post_permission( $post, 'edit' ) ) {
            $post_fields_extended['password'] = $post['post_password'];
        }

        // Consider future posts as published
        if ( $post_fields['status'] === 'future' ) {
            $post_fields['status'] = 'publish';
        }

        // Fill in blank post format
        $post_fields['format'] = get_post_format( $post['ID'] );

        if ( empty( $post_fields['format'] ) ) {
            $post_fields['format'] = 'standard';
        }

        if ( 0 === $post['post_parent'] ) {
            $post_fields['parent'] = null;
        }

        if ( ( 'view' === $context || 'view-revision' == $context ) && 0 !== $post['post_parent'] ) {
            // Avoid nesting too deeply
            // This gives post + post-extended + meta for the main post,
            // post + meta for the parent and just meta for the grandparent
            $parent = get_post( $post['post_parent'], ARRAY_A );
            $post_fields['parent'] = $this->prepare_post( $parent, 'embed' );
        }

        // Merge requested $post_fields fields into $_post
        $_post = array_merge( $_post, $post_fields );

        // Include extended fields. We might come back to this.
        $_post = array_merge( $_post, $post_fields_extended );

        if ( 'edit' === $context ) {
            if ( json_check_post_permission( $post, 'edit' ) ) {
                $_post = array_merge( $_post, $post_fields_raw );
            } else {
                $GLOBALS['post'] = $previous_post;
                if ( $previous_post ) {
                    setup_postdata( $previous_post );
                }
                return new WP_Error( 'json_cannot_edit', __( 'Sorry, you cannot edit this post' ), array( 'status' => 403 ) );
            }
        } elseif ( 'view-revision' == $context ) {
            if ( json_check_post_permission( $post, 'edit' ) ) {
                $_post = array_merge( $_post, $post_fields_raw );
            } else {
                $GLOBALS['post'] = $previous_post;
                if ( $previous_post ) {
                    setup_postdata( $previous_post );
                }
                return new WP_Error( 'json_cannot_view', __( 'Sorry, you cannot view this revision' ), array( 'status' => 403 ) );
            }
        }

        // Entity meta
        $links = array(
            'self'       => json_url( '/posts/' . $post['ID'] ),
            'author'     => json_url( '/users/' . $post['post_author'] ),
            'collection' => json_url( '/posts' ),
        );

        if ( 'view-revision' != $context ) {
            $links['replies'] = json_url( '/posts/' . $post['ID'] . '/comments' );
            $links['version-history'] = json_url( '/posts/' . $post['ID'] . '/revisions' );
        }

        $_post['meta'] = array( 'links' => $links );

        if ( ! empty( $post['post_parent'] ) ) {
            $_post['meta']['links']['up'] = json_url( '/posts/' . (int) $post['post_parent'] );
        }

        $GLOBALS['post'] = $previous_post;
        if ( $previous_post ) {
            setup_postdata( $previous_post );
        }
        return apply_filters( 'json_prepare_post', $_post, $post, $context );
    }

    public function get_category_recent_posts( $post_id ) {
        $recent_posts = array();

        $post_cats = get_the_category($post_id);
        $post_cat_id = $post_cats[0]->cat_ID;

        $args = array(
            'cat' => $post_cat_id,
            'post_type' => 'post',
            'post_per_page' => '1',
        );

        $query = new WP_Query($args);

        $index = 0;
        while ($query->have_posts()) {
            if ($index > 3) break;
            
            $query->the_post();
            $post = get_post();

            $post->post_author = pods_field($post->post_type, $post->ID, 'custom_author', true);
            $post->featured_image = new stdClass();
            $featured_image_id = get_post_thumbnail_id($post->ID);
            $post->featured_image->url = wp_get_attachment_url($featured_image_id) ? wp_get_attachment_url($featured_image_id) : '';

            if ($post->ID != $post_id) {
                $recent_posts[] = $post;
            }       

            $index++;
        } 

        return $recent_posts;
    }

    public function get_global_recent_posts( $post_id ) {
        $recent_posts = array();
        $args = array(
            'post_type' => 'post',
            'post_per_page' => '1',
        );

        $query = new WP_Query($args);
        $index = 0;
        
        while ($query->have_posts()) {
            if ($index > 3) break;  
            
            $query->the_post();
            $post = get_post();

            $post->post_author = pods_field($post->post_type, $post->ID, 'custom_author', true);
            $post->featured_image = new stdClass();
            $featured_image_id = get_post_thumbnail_id($post->ID);
            $post->featured_image->url = wp_get_attachment_url($featured_image_id) ? wp_get_attachment_url($featured_image_id) : '';

            if ($post->ID != $post_id) {
                $recent_posts[] = $post;
            }

            $index++;
        } 

        return $recent_posts;
    }

    function get_query() {
        global $wp;

        $query = array();

        foreach ($wp->public_query_vars as $var) {
            if (get_query_var($var))
                $query[$var] = get_query_var($var);
        }
        foreach ($wp->private_query_vars as $var) {
            if (get_query_var($var))
                $query[$var] = get_query_var($var);
        }


        if (!isset($query['post_type']))
            $query['post_type'] = array('post', 'custom_author');

        return $query;
    }

    function format_post($post) {
        $post->human_time = human_time_diff( get_the_time('U', $post->ID), current_time('timestamp') ) . ' ago';
        $post->permalink = get_permalink($post->ID);

        $post->featured_image = new stdClass();
        $featured_image_id = get_post_thumbnail_id($post->ID);
        $post->featured_image->url = wp_get_attachment_url($featured_image_id);
        //$post->featured_image->full = wp_get_attachment_image_src($featured_image_id, 'full');

        $post->author_displayname = get_the_author_meta('display_name', $post->post_author);

        $post->terms = new stdClass();
        $post->terms->category = (object)wp_get_post_terms($post->ID, 'category');
        $post->terms->post_tag = (object)wp_get_post_terms($post->ID, 'post_tag');


        if ($post->terms->category) {
            foreach($post->terms->category as $cat) {
                $post->terms->category_first_item = $cat;
            }
        }

        if ($post->terms->post_tag) {
            foreach($post->terms->post_tag as $tag) {
                $post->terms->post_tag_first_item = $tag;
            }
        }

        $post->hash_tag = '';

        $post->post_content = do_shortcode($post->post_content);
        $post->skus = pods_field($post->post_type, $post->ID, 'sku');

        return $post;
    }

    /**
     * Retrieve the post excerpt.
     *
     * @return string
     */
    protected function prepare_excerpt( $excerpt ) {
        if ( post_password_required() ) {
            return __( 'There is no excerpt because this is a protected post.' );
        }

        $excerpt = apply_filters( 'the_excerpt', apply_filters( 'get_the_excerpt', $excerpt ) );

        if ( empty( $excerpt ) ) {
            return null;
        }

        return $excerpt;
    }

    function object_to_array($object) {
        return json_decode(json_encode($object), true);
    }
}