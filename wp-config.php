<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mdwordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'js>J)PLJ@d|TZ: pwM}2Jb5h$>~cLO!Ho![A~dZ>d6OZakO,3k2FstqQ+z81schZ');
define('SECURE_AUTH_KEY',  'XQA6@3O>x]?Um/ot`) ?,AE;(:Jos{ jXEWYI(,E*d~#WfL3ML2 x^~Rd{y0OTTb');
define('LOGGED_IN_KEY',    'AonPn$v*bd1%:n)|`F$/fe`x9CD$x*~[~txQb&rbK1fZwe#kxw)th#a$w.3uL%vU');
define('NONCE_KEY',        '%7~>N~5kzX,{l88u@oa=-+~q<d6UiBN:$E<9V&ibm~37`kT*/2>~+q25sr+C:$c`');
define('AUTH_SALT',        'ri-Iug`V< ]8xJ#:cOa<p^R#^^w%6=cK(}+IYI#IxT| Q(zO7A)/QvK0SvzJZ6LF');
define('SECURE_AUTH_SALT', 'wDk,m`{(#D@K_dB(TcXvY(lNZ/Z>fkVP? wWy`QhQB=Md/L(`cc9,y>^.7I^S-:z');
define('LOGGED_IN_SALT',   'TDoW$#8[~LB+4M3su)AI`X2g<@xE2G}!hnZMR0LjR.4q`B8;FzD?>bc@RY>9B+,F');
define('NONCE_SALT',       'Qd=rPN|&^e;_}OkQ>s`?(4cf$Pr*8jJ;s.M>ZXY-^3+w9F4z3.j]!VkBDOQoTv&f');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

